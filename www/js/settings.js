var app_settings = new Vue({
    el: '#vue-settings',
    data: {
    },
    methods: {
        resetName: function () {
            localStorage.username = "";
            goStartPage();
        }
    }
})

function goStartPage() {
    window.location = "start_page.html";
}