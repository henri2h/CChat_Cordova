if (localStorage.username != "") {
    var vue = new Vue({
        el: '#app',
        data: {

            selectedConv: null,
            convs: [],
            version: localStorage.version,
            message: "Loading conversation..."

        },
        methods: {
            clickedPlace(id) {
                this.selectedConv = event.target.innerHTML
                localStorage.convID = id;
                localStorage.convName = event.target.innerHTML;
                changePage();
            }
        }
    })

    loadConversationsList().then(convs=>{
        if (convs.length == 0) {
            vue.message = "Problem loading messages";
        }
        else {
            vue.convs = convs;
            vue.message = "";
        }
    })

   


}
else {
    goToLogin();
}

function changePage() {
    window.location = "conv_view.html";
}
function goToLogin() {
    window.location = "start_page.html";
}