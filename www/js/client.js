var host = "https://henri2h.fr:8443";

function setConversationName(convName) {
  fetch(host + "/Conversation/SetConversationName", {
    "method": "POST",
    "headers": {
      "content-type": "application/json",
      "token": localStorage.token,
      "username": localStorage.username
    },

    // TODO : WARNING : BUG TO CORRECT
    "body": JSON.stringify({
      "ConvName": localStorage.convID,
      "ConvID": convName
    })
  })
    .then(response => {
      console.log(response);
      localStorage.convName = convName;
    })
    .catch(err => {
      console.log(err);
    });
}

function sendMessage(text) {
  console.log("Going to send message");
  var messageComposer = document.querySelector("#message-composer");

  fetch(host + "/Conversation/SendConversationMessage", {
    "method": "POST",
    "headers": {
      "content-type": "application/json",
      "token": localStorage.token,
      "username": localStorage.username
    },
    "body": JSON.stringify({
      "ConvID": convID,
      "Content": text,
      "DataType": "Text"
    })
  })
    .then(response => {
      // message send, we can reload
      loadMessages(false, true);
    })
    .catch(err => {
      console.log(err);
    });
}

async function loadConversationsList() {
  var results = Array();

  await fetch(host + "/Conversation/ListConversations", {
    "method": "POST",
    "headers": {
      "content-type": "multipart/form-data; boundary=---011000010111000001101001",
      "token": localStorage.token,
      "username": localStorage.username
    }
  })
    .then(response => {
      //console.log(response);
      return response.json();
    })
    .then((result) => {
      console.log(result);
      result.forEach(element => {
        var conv = {}
        conv["id"] = element["ConvID"];
        conv["name"] = element["ConvName"];
        results.push(conv);
      });
    })
    .catch(err => {
      console.log(err);
    });
  return results;
}

function connect(device_id) {
  fetch(host + "/V2.0/Account/connection", {
    method: "POST",
    headers: {
      "content-type": "application/json",
      "username": localStorage.username,
      "password": localStorage.password,
      "device_id": device_id
    },
    body: ""
  })
    .then(response => {
      console.log(response);
      localStorage.token = response;
    });
}


async function GetPicture(convID, fileName) {
  return await fetch(host + "/Conversation/GetPicture", {
    "method": "POST",
    "headers": {
      "content-type": "application/json",
      "token": localStorage.token,
      "username": localStorage.username
    },
    "body": JSON.stringify({
      "convID": convID,
      "fileName": fileName
    })
  })
    .then(response => {
      return response.blob();
    })
    .catch(err => {
      console.log("Err : " + err);
      return err;
    });
}

async function SendPicture(convID, file) {
  return await fetch(host + "/Conversation/SendPicture", {
    "method": "POST",
    "headers": {
      "content-type": file.type,
      "token": localStorage.token,
      "username": localStorage.username,
      "convID": convID
    },
    body:file
  })
    .then(response => {
      console.log(response);
      return response;
    })
    .catch(err => {
      console.log(err);
      return err;
    });
}