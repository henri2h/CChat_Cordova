
var app = {
    // Application Constructor
    initialize: function () {
        // document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function () {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function (id) {
        /*var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        eceivedElement.setAttribute('style', 'display:block;');*/

        console.log('Received Event: ' + id);
    }

};

app.initialize();

document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {


    function onPrompt(results) {
        alert("You selected button number " + results.buttonIndex + " and entered " + results.input1);
    }

    /*navigator.notification.prompt(
        'Please enter your name',  // message
        onPrompt,                  // callback to invoke
        'Registration',            // title
        ['Ok','Exit'],             // buttonLabels
        'Jane Doe'                 // defaultText
    );
    //navigator.notification.beep(3);
*/
    /*cordova.plugins.notification.local.schedule({
        title: 'My first notification',
        text: 'Thats pretty easy...',
        foreground: true
    });*/


    document.addEventListener('deviceready', function () {
        // cordova.plugins.backgroundMode is now available

        console.log("device is ready");
        // enable background mode

        /*
        // 1) Request background execution
        cordova.plugins.backgroundMode.enable();

        //cordova.plugins.backgroundMode.disableWebViewOptimizations();
        cordova.plugins.backgroundMode.overrideBackButton();
        //cordova.plugins.backgroundMode.excludeFromTaskList();
        console.log("Background mode enabled");

        // 2) Now the app runs ins background but stays awake
        cordova.plugins.backgroundMode.on('activate', function () {
            console.log("Background mode activated");

            setInterval(function () {
                cordova.plugins.notification.badge.increase();
                //navigator.notification.beep(1);
                console.log("interval");
                cordova.plugins.notification.local.schedule({
                    title: "in background",
                    text: element["Content"],
                    foreground: true
                  });
    }, 1000);

        });

        // 3) App is back to foreground
        cordova.plugins.backgroundMode.on('deactivate', function () {
            cordova.plugins.notification.badge.clear();
        });
        */

        console.log("Device ready end");
        const push = PushNotification.init({
            android: {
            },
            ios: {
                alert: "true",
                badge: "true",
                sound: "true"
            },
            windows: {}
        });

        push.on('notification', (data) => {
            console.log("data :", data);
            // data.message,
            // data.title,
            // data.count,
            // data.sound,
            // data.image,
            // data.additionalData

            cordova.plugins.notification.local.schedule({
                title: data.title,
                text: data.message,
                foreground: true
            });
        });

        push.on('error', (e) => {
            // e.message
            console.log("error : ", e);
        });

        push.on('registration', (data) => {
            // data.registrationId
            console.log(data.registrationId);
            connect(data.registrationId);
        });

        push.on('error', function (e) {
            console.log(e.message)
        });

        console.log("registered");

        var success = function() {
            console.log("Event tracked");
        }
        
        var error = function(error) {
            console.error(error);
        }

        AppCenter.Analytics.trackEvent('App started', { User: localStorage.username});
    }, false);
}