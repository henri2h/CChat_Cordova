var host = "https://henri2h.fr:8443/";

// load data
var username = localStorage.username;
var convID = localStorage.convID;
var ConvName = localStorage.convName;

var lastMessage = 0;

// global
// TODO :  move to vue.js
document.querySelector(".data-convName").textContent = ConvName;

function displayMessages(convID, firstRun, shouldScroll) {
  if ((localStorage.getItem["conv_" + convID] === null) == false) {
    console.log("Display messages : ");
    // On vérifie si le navigateur prend en charge
    // l'élément HTML template en vérifiant la présence
    // de l'attribut content pour l'élément template.
    if ("content" in document.createElement("template")) {

      // On prépare une ligne pour le tableau 
      var template = document.querySelector("#data_message");

      // On clone la ligne et on l'insère dans le tableau
      var tbody = document.querySelector("#messages");
      // we remove every element already in it
      while (tbody.firstChild) {
        tbody.removeChild(tbody.firstChild);
      }

      JSON.parse(localStorage.getItem("conv_" + convID))["Messages"].forEach(element => {
        var clone = document.importNode(template.content, true);

        var mess_username = clone.querySelector(".data_message_username");
        mess_username.textContent = element["Username"];

        var mess_senddate = clone.querySelector(".data_message_senddate");
        mess_senddate.textContent = element["SendDate"];


        if (element["DataType"] == "Text") {
          var mess_text = clone.querySelector(".data_message_text");
          mess_text.textContent = element["Content"];
        }
        if (element["DataType"] == "Image") {
          var mess_image_figure = clone.querySelector(".image");
          mess_image_figure.classList.remove("is-hidden");

          var mess_image = clone.querySelector(".data_image");
          mess_image.id = "image_" + element["Content"];

          var mess_progress = clone.querySelector("progress");


          GetPicture(localStorage.convID, element["Content"]).then(function (myBlob) {
            console.log(myBlob);
            var objectURL = URL.createObjectURL(myBlob);
            mess_image.src = objectURL;
            mess_progress.classList.add("is-hidden");
          });
        }

        if (element["Username"] == username) {
          //clone.children[0].classList.add("has-background-primary");
          clone.querySelector(".box").classList.add("has-background-primary");
          clone.querySelector(".column").classList.add("is-offset-7");
        }


        // should scroll if new message
        // we have recieved a new message
        if (element["MessageID"] > lastMessage) {

          lastMessage = element["MessageID"];
          if (shouldScroll == false) clone.querySelector(".box").classList.add("has-background-warning");
          shouldScroll = true;


          // detect if new message has been recieved
          if (firstRun == false && username != element["Username"]) {
            console.log(username + " " + element["Username"])
            console.log("New message recieved : " + element["MessageID"] + " " + lastMessage);
            //navigator.notification.beep(1);

            /*cordova.plugins.notification.local.schedule({
              title: element["Username"] + "@" + ConvName,
              text: element["Content"],
              foreground: true
            });*/
          }
        }
        tbody.appendChild(clone);
      });

      if (shouldScroll) {
        // got to the end of the page
        window.scrollTo(0, document.body.scrollHeight);
      }


    } else {
      // Une autre méthode pour ajouter les lignes
      // car l'élément HTML n'est pas pris en charge.
      console.log("Error ...");
    }
  }
  else {
    console.log("Conversation : " + convID + " has not been downloaded yet");
  }
}

function loadMessages(firstRun, shouldScroll) {
  if (convID == "" && username == "") {
    console.log("Username or convID not set could not continue");
    return;
  }

  fetch(host + "/Conversation/GetConversation", {
    method: "POST",
    headers: {
      "content-type": "application/json",
      "token": "token here",
      "username": username
    },
    body: JSON.stringify({
      "ConvID": convID
    })
  })
    .then(response => {
      //console.log(response);
      return response.text();
    })
    .then((responseData) => {


      // check if we should update the conv

      if ((localStorage.getItem["conv_" + convID] === null) == false) {
        var oldconv = JSON.parse(localStorage.getItem("conv_" + convID));
        var newconv = JSON.parse(responseData);
        // detect if there is new messages
        if (oldconv["Messages"].length == newconv["Messages"].length) {
          console.log("No new messages");
          return responseData; // directly return no need to update
        }
      }

      // we should update data
      localStorage.setItem("conv_" + convID, responseData);
      displayMessages(convID, false, false);

      return responseData;
    })
    .catch(err => {
      console.log(err);
    });
}

function sendMessage() {
  console.log("Going to send message");
  var messageComposer = document.querySelector("#message-composer");

  if (messageComposer.value != "") {
    fetch(host + "/Conversation/SendConversationMessage", {
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "token": "token here",
        "username": username
      },
      "body": JSON.stringify({
        "ConvID": convID,
        "Username": username,
        "Content": messageComposer.value,
        "DataType": "Text"
      })
    })
      .then(response => {
        // message send, we can reload
        loadMessages(false, true);
      })
      .catch(err => {
        console.log(err);
      });

    messageComposer.value = "";
  }
}

// it is first run
displayMessages(convID, true, true);

// send message on enter
// Get the input field
var input = document.querySelector("#message-composer");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function (event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    sendMessage();
  }
});


// refresh ui

function updateUI() {
  loadMessages(false, false);
}

// load new messages every 3 s
window.setInterval(updateUI, 3000);

// page ui
function sendPictureDisplaySelection() {
  document.querySelector("#input_image").click();
}

document.querySelector("#input_image").addEventListener('change', loadPicture);

function loadPicture() {
  var curFiles = document.querySelector("#input_image").files;
  if (curFiles.length === 0) {
    Console.log('No files currently selected for upload');
  } else {
    for (var i = 0; i < curFiles.length; i++) {
      console.log(curFiles[i]);

      imageConversion.compressAccurately(curFiles[i], 200).then(resfile => {
        //The res in the promise is a compressed Blob type (which can be treated as a File type) file;
        SendPicture(localStorage.convID, resfile).then(result => {
          console.log("Image sent");
          updateUI();
        })
      })
      //image.src = window.URL.createObjectURL(curFiles[i]);
    }

  }
}